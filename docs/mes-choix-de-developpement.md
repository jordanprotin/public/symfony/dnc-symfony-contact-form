<div align="center" style="margin-bottom: 30px;">
    <img src="https://www.dansnoscoeurs.fr/images/logo/logo-dnc.png?ea266cace534b37831dca86a8987b5e6cb2ca4e6" alt="logo-dnc">
</div>

> Auteur: Jordan Protin | jordan.protin@yahoo.com
 
# Mes choix de développement

<div align="justify">
    Je vais reprendre chaque point énoncé dans les consignes de <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/docs/pdf/exercice_developpeur_symfony.pdf" title="pdf-exo">cet exercice technique</a> et décrire comment j'ai pu y répondre.
</div>

# Sommaire
  - [1. Création d'un projet Symfony](#1-cr%c3%a9ation-dun-projet-symfony)
  - [2. Champs à implémenter](#2-champs-%c3%a0-impl%c3%a9menter)
    - [2.1. Comment ai-je procédé ?](#21-comment-ai-je-proc%c3%a9d%c3%a9)
      - [A) Création de l'entité](#a-cr%c3%a9ation-de-lentit%c3%a9)
      - [B) Création du controller](#b-cr%c3%a9ation-du-controller)
      - [C) Création du template de base](#c-cr%c3%a9ation-du-template-de-base)
      - [D) Création du template du formulaire de contact](#d-cr%c3%a9ation-du-template-du-formulaire-de-contact)
      - [E) Création du layout pour les formulaires](#e-cr%c3%a9ation-du-layout-pour-les-formulaires)
      - [F) Création des fichiers de traductions](#f-cr%c3%a9ation-des-fichiers-de-traductions)
      - [G) Création du formulaire de contact](#g-cr%c3%a9ation-du-formulaire-de-contact)
      - [H) Gestion des régles de validation](#h-gestion-des-r%c3%a9gles-de-validation)
    - [2.2. Vos coordonnées](#22-vos-coordonn%c3%a9es)
      - [A) Nom: obligatoire, entre 2 et 100 caractères](#a-nom-obligatoire-entre-2-et-100-caract%c3%a8res)
      - [B) Prénom: obligatoire, entre 2 et 100 caractères](#b-pr%c3%a9nom-obligatoire-entre-2-et-100-caract%c3%a8res)
      - [C) Téléphone: non obligatoire par défaut. Format numérique, 10 chiffres avec un masque de saisie pour les numéros de téléphone au format français](#c-t%c3%a9l%c3%a9phone-non-obligatoire-par-d%c3%a9faut-format-num%c3%a9rique-10-chiffres-avec-un-masque-de-saisie-pour-les-num%c3%a9ros-de-t%c3%a9l%c3%a9phone-au-format-fran%c3%a7ais)
      - [D) Email: adresse email valide](#d-email-adresse-email-valide)
    - [2.3. L'objet de la demande](#23-lobjet-de-la-demande)
      - [A) Liste déroulante avec les choix suivants](#a-liste-d%c3%a9roulante-avec-les-choix-suivants)
      - [B) Champ textarea pour saisir le message (entre 10 et 500 caractères)](#b-champ-textarea-pour-saisir-le-message-entre-10-et-500-caract%c3%a8res)
      - [C) Quand souhaitez-vous être rappelé ?*](#c-quand-souhaitez-vous-%c3%aatre-rappel%c3%a9)
  - [3. Validation](#3-validation)
  - [4. Informations complémentaires](#4-informations-compl%c3%a9mentaires)

## 1. Création d'un projet Symfony

<div align="justify" class="section">
    J'ai donc utilisé la toute dernière version stable du framework <strong>Symfony</strong> (la version <span>5.0.*</span>).
    Pour initialiser mon projet, j'ai utilisé la version la plus <em>light</em> possible de manière à devoir installer uniquement ce dont j'avais besoin pour la réalisation de cet exercice. J'ai donc utilisé la commande suivante :
</div>

```bash
$ composer create-project symfony/skeleton dnc-symfony-contact-form
```

## 2. Champs à implémenter

### 2.1. Comment ai-je procédé ?

Pour construire ce formulaire de contact, j'ai réalisé ces différentes étapes :

#### A) Création de l'entité

<div align="justify" class="section">
    Il s'agit de <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité</a> que j'ai logiquement nommé <span>Contact</span>. Cette dernière comporte donc les différentes propriétés nécessaires et les <em>getters/setters</em> associés pour pouvoir les manipuler. J'ai également implémenté dans ce fichier <em>2 constantes</em> me permettant de lister les items en ce qui concerne les propriétés <strong>subject</strong> et <strong>availability</strong> :
</div>

```php
// App\Entity\Contact.php

// line ... 17
const SUBJECTS = [
    0 => 'Autre',
    1 => 'Contacter le service commercial',
    2 => 'Contacter le service facturation',
    3 => 'Faire une demande sur une association ou un projet solidaire',
    4 => 'Proposer une fonctionnalité',
    5 => 'Je signale un problème technique',
    6 => 'Faire une remarque sur le site',
    7 => 'Être rappelé',
];

const AVAILABILITIES = [
    0 => 'Dès que possible',
    1 => 'Le matin',
    2 => 'Le midi',
    3 => 'L\'après-midi'
];
```

> Ces constantes me permettent de centraliser en un seul endroit ces listes d'items et ainsi pouvoir les modifier aisément si besoin. 

> Je reviendrai sur les règles d'assertion que j'ai pu attribuer à chaque propriété (pour la validation des champs).

> Entité qui n'a nullement besoin d'être mappée à une BDD via [doctrine](https://symfony.com/doc/current/doctrine.html) dans le cadre de cet exercice.

#### B) Création du controller

<div align="justify" class="section">
    <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Controller/ContactController.php" title="src/Controller/ContactController.php">Ce controller</a> permet de construire notre formulaire, retourner ce dernier à la vue associée puis d'effectuer la validation du formulaire selon les règles établies à chaque soumission. Dès lors, un traitement particulier sera effectué en cas de succès (comme afficher un message flash de confirmation dans notre cas).
</div>

#### C) Création du template de base

<div align="justify" class="section">
    Ce <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/templates/base.html.twig" title="templates/base.html.twig">template de base</a> permet, entre autre, de réduire le code en évitant la duplication de code et en centralisant les élements communs de cette mini application web. De fait, chacun des autres templates doivent l'étendre pour avoir, par exemple, le même header sur chaque page,  utiliser le même rendu en ce qui concerne les messages flash, la gestion dynamique des titres et des des metas descriptions des pages, etc. 
</div>

> Pour construire et gérer les templates, j'utilise le moteur de template [twig](https://twig.symfony.com/). Je suis donc passé par [symfony/twig-pack](https://github.com/symfony/twig-pack) pour l'utiliser dans l'application. 

> J'y reviendrai un peu plus tard, mais j'ai utilisé [Webpack Encore](https://symfony.com/doc/current/frontend.html) pour gérer l'ensemble des fichiers statiques de l'application.

#### D) Création du template du formulaire de contact

<div align="justify" class="section">
    Pour construire <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/templates/contact/index.html.twig" title="templates/contact/index.html.twig">ce template</a>, j'ai activement utilisé <a href="https://getbootstrap.com/" title="bootstrap">bootstrap</a>.
</div>

#### E) Création du layout pour les formulaires

<div align="justify" class="section">
    <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/tree/master/templates/form" title="templates/form">Ce layout</a> garantie que chaque formulaire de l'application se calque sur ce modèle (bien qu'un seul subsiste dans le cadre de cet exercice). De fait, chaque formulaire aura le même rendu. Cela permet aussi d'éviter la duplication de code (comme devoir dupliquer à chaque fois dans chaque formulaire les blocs <em>HTML</em> pour afficher les inputs, les labels, etc.). Ce layout est donc <em>connu</em> de l'application en ajoutant la propriété <span>form_themes</span> dans le fichier <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/config/packages/twig.yaml" title="config/packages/twig.yaml">config/packages/twig.yaml</a> :
</div>

```yaml
# config/packages/twig.yaml

twig:
    default_path: '%kernel.project_dir%/templates'
    debug: '%kernel.debug%'
    strict_variables: '%kernel.debug%'
    exception_controller: null
    form_themes:
        - 'form/layout.html.twig'
        - 'form/fields.html.twig'
```

> Ainsi, chaque formulaire de l'application étendra le style des formulaires de bootstrap.

#### F) Création des fichiers de traductions

<div align="justify" class="section">
    Pour se faire, j'ai utilisé <a href="https://symfony.com/doc/current/translation.html" title="symfony/translation">symfony/translation</a>. En effet, j'utilise systématiquement ce système sur chacun de mes projets afin de centraliser en un seul endroit l'ensemble de mes <em></em>wording</em>. Il sera par conséquent rapide de les modifier si besoin.
</div>

> Pour améliorer la visiblité, j'ai favorisé l'utilisation du format `yaml`.

#### G) Création du formulaire de contact

<div align="justify" class="section">
    Il s'agit <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Form/ContactType.php" title="src/Form/ContactType.php">du formulaire</a> qui est rendu dans la vue. Pour le construire, j'ai évidemment utilisé <a href="https://symfony.com/doc/current/forms.html" title="symfony/form">symfony/form</a>.
</div>

> Je reviendrai sur le contenu de ce fichier plus en détails par la suite.

#### H) Gestion des régles de validation

<div align="justify" class="section">
    Pour gérer les règles de validation sur chaque propriété du formulaire, je suis passé par <a href="https://symfony.com/doc/current/validation.html" title="symfony/validator et doctrine/annotations">symfony/validator et doctrine/annotations</a>.
</div>

> Je reviendrai plus en détails sur l'utilisation de ces derniers par la suite.

### 2.2. Vos coordonnées

> Parce que ce n'est pas le but de cet exercice, j'ai manuellement désactivé la validation HTML5 (côté client) pour tester la bonne gestion des règles de validation (côté serveur). Pour ce faire, j'ai simplement ajouté ce bloc de code dans le fichier [src/Form/ContactType.php](https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Form/ContactType.php) :

```php
// App\Form\ContactType.php

// ... line 137
'attr' => [
    'novalidate' => 'novalidate' // comment me to reactivate the html5 validation!
]
```

#### A) Nom: obligatoire, entre 2 et 100 caractères

<div align="justify" class="section">
    J'ai utilisé <em>la force</em> des annotations pour ce faire. Dans <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité</a>, voici ce que j'ai ajouté sur la propriété <span>lastname</span> :
</div>

```php
// App\Entity\Contact.php

// ... line 43
/**
 * @var string|null
 * 
 * @Assert\NotBlank(message="form.entity.contact.blank.lastname")
 * @Assert\Length(min=2, max=100)
 */
private $lastname;
```

<div align="justify" class="section">
    J'ai également ajouté dans le fichier <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Form/ContactType.php" title="src/Form/ContactType.php">src/Form/ContactType.php</a> les <span>attributs HTML</span> nécessaires pour aussi gérer la validation côté client :
</div>

```php
// App\Form\ContactType.php

// ... line 50
->add('lastname', TextType::class, [
    'label' => 'form.entity.contact.label.lastname',
    'attr' => [
        'minlength' => 2,
        'maxlength' => 100,
    ],
])
```

#### B) Prénom: obligatoire, entre 2 et 100 caractères

<div align="justify" class="section">
    Même chose que pour le <span>nom</span>.
</div>

#### C) Téléphone: non obligatoire par défaut. Format numérique, 10 chiffres avec un masque de saisie pour les numéros de téléphone au format français

<div align="justify" class="section">
    Pour cette règle, j'ai construit <em>ma propre règle</em> en utilisant le paquet <a href="https://github.com/google/libphonenumber" title="google/libphonenumber">google/libphonenumber</a> proposé par google. De fait, j'ai ajouté ma règle sur la propriété <span>phone</span> de <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité Contact</a> :
</div>

```php
// App\Entity\Contact.php

// ... line 5
use App\Validator\Constraints as AppAssert;

// ... line 51
/**
 * @var string|null
 * 
 * @AppAssert\Phone()
 */
private $phone;
```

<div align="justify" class="section">
    J'ai donc crée un nouveau dossier <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/tree/master/src/Validator/Constraints" title="Validator/Constraints">Validator/Constraints</a> incluant ma règle de validation <span>personnalisée</span> pour vérifier que le numéro de téléphone renseigné est bien au format français.
</div>

> Même si le numéro respecte le format français, la librairie [google/libphonenumber](https://github.com/google/libphonenumber) retournera faux si le numéro de téléphone n'existe pas dans l'annuaire français.

<div align="justify" class="section">
    Enfin, pour gérer le masque de saisie, j'ai utilisé le paquet <a href="https://github.com/RobinHerbots/Inputmask" title="inputmask">inputmask</a>. Ainsi, dans mon script javascript, j'ai pu appliquer le masque de saisie côté client dans le fichier <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/assets/js/app.js" title="assets/js/app.js"></a> comme ceci :
</div>

```js
// assets/js/app.js

// ... line 21
import Inputmask from 'inputmask';

// ... line 25
$(document).ready(function() {
    Inputmask({'mask': '09.99.99.99.99'}).mask('#contact_phone');
});
```

> Je suis donc passé par [Webpack Encore](https://symfony.com/doc/current/frontend.html) pour builder mes assets et les appeler dans mes vues. J'ai donc également installé [jQuery](https://jquery.com/) sur le projet.

#### D) Email: adresse email valide

<div align="justify" class="section">
    Pour cela, j'ai utilisé l'annotation de contrainte <span>Email</span> dans <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité Contact</a>:
</div>

```php
// App\Entity\Contact.php

// ... line 58
/**
 * @var string|null
 * 
 * @Assert\NotBlank(message="form.entity.contact.blank.email")
 * @Assert\Email()
 */
private $email;
```

### 2.3. L'objet de la demande

#### A) Liste déroulante avec les choix suivants

<div align="justify" class="section">
    Comme dit précédement, j'ai donc implémenté une constante <span>SUBJECT</span> dans <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité Contact</a>. De fait, dans le <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Controller/ContactController.php" title="src/Controller/ContactController.php">controller Contact</a>, je récupère ma liste d'items et je la passe en options de mon formulaire pour la restituer dans la liste déroulante :
</div>

```php
// App\Controller\ContactController.php

// ... line 66 
/**
 * Get the contact subject items 
 * in order to populate the dropdown list
 */
private function getSubjectChoices()
{
    $slugger = new AsciiSlugger();
    $choices = Contact::SUBJECTS;

    $output = [];

    foreach($choices as $v) {
        // ex: ['Proposer une fonctionnalité] = 'proposer-une-fonctionnalite'
        $output[$v] = (string) $slugger->slug($v)->lower();
    }

    return $output;
}
```

<div align="justify" class="section">
    J'ai utilisé la classe <span>AsciiSlugger</span> du paquet <a href="https://symfony.com/doc/current/components/string.html" title="symfony/string">symfony/string</a> pour générer des slugs en tant que clés de chaque item. Cela est utile lorsque l'utilisateur saisie directement dans l'URL le sujet. <br> Par exemple : <a href="https://www.dansnoscoeurs.fr/contact/proposer-une-fonctionnalite" title="dnc-contact">https://www.dansnoscoeurs.fr/contact/proposer-une-fonctionnalite</a>. De fait, je peux <em>setter</em> automatiquement le sujet de mon entité Contact et ainsi le formulaire mettra à jour l'item de la liste déroulante automatiquement. Par défaut, ce sera le sujet <span>Autre</span>.
</div>

```php
// App\Controller\ContactController.php

// ... line 27
public function index(Request $request, string $subject = 'autre'): Response
{
    // ... line 32
    $subjectChoices = $this->getSubjectChoices();

    // ... line 47
    $form = $this->createForm(ContactType::class, $contact, [
        'subjectChoices' => $subjectChoices
    ]);
}
```

<div align="justify" class="section">
    Ainsi, dans le fichier construisant le formulaire, je peux récupérer la liste et l'injecter dans la liste déroulante :
</div>

```php
// App\Form\ContactType.php

// ... line 68
->add('subject', ChoiceType::class, [
    'label' => 'form.entity.contact.label.subject',
    'attr' => [
        'class' => 'contact-form-subject'
    ],
    'choices' => $options['subjectChoices'],
])
```

#### B) Champ textarea pour saisir le message (entre 10 et 500 caractères)

<div align="justify" class="section">
    Pour cela, j'ai utilisé l'annotation de contrainte <span>Length</span> dans <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité Contact</a>:
</div>

```php
// App\Entity\Contact.php

// ... line 76
/**
 * @var string|null
 * 
 * @Assert\NotBlank(message="form.entity.contact.blank.message")
 * @Assert\Length(min=10, max=500)
 */
private $message;
```

<div align="justify" class="section">
    J'ai également utilisé la classe <span>TextareaType</span> pour calquer ce champ sur un textarea :
</div>

```php
// App\Form\ContactType.php

// ... line 10
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

// ... line 75
->add('message', TextareaType::class, [
    'label' => 'form.entity.contact.label.message',
    'attr' => [
        'rows' => 9,
        'minlength' => 10,
        'maxlength' => 500,
    ],
])
```

#### C) Quand souhaitez-vous être rappelé ?*

> Si l'item « Etre rappelé » est sélectionné, recharger le formulaire pour afficher les cases à cocher.

> Au moins une case doit être sélectionnée.

> Le champ téléphone doit être obligatoire dans ce cas précis, il est facultatif sinon.

Pour répondre à cette problématique, j'ai utilisé :

- [jQuery.ajax()](https://api.jquery.com/jquery.ajax/) 
  
<div align="justify" class="section">
    J'intercèpte l'item <span>subject</span> à chaque fois que l'utilisateur sélectionne un sujet dans la liste déroulante. Dès lors, j'effectue une requête vers le controller qui va reconstruire le formulaire en fonction de l'élément sélectionné.
</div>

- [Form Events](https://symfony.com/doc/current/form/dynamic_form_modification.html)

<div align="justify" class="section">
    Si le sujet sélectionné est <strong>Etre rappelé</strong>, j'ajoute alors dynamiquement un champ dans mon formulaire. Le bloc cases à cocher en l'occurence :
</div>

```php
// App\Form\ContactType.php

// ... line 85
$formModifier = function (FormInterface $form, string $subject = null) {
    $availabilities = null === $subject ? [] : $this->getAvailabilityChoices();

    if ($subject === 'etre-rappele') {
        $form
            ->add('availability', ChoiceType::class, [
                'choices' => $availabilities,
                'multiple' => true,
                'expanded' => true,
                'label' => 'form.entity.contact.label.availability',
                'constraints' => [
                    new Assert\Count([
                        'min' => 1, 
                        'minMessage' => 'form.entity.contact.blank.availability'
                    ])
                ],
            ])
        ;
    }
};

$builder->addEventListener(
    FormEvents::PRE_SET_DATA,
    function (FormEvent $event) use ($formModifier) {
        $data = $event->getData();

        $formModifier($event->getForm(), $data->getSubject());
    }
);

$builder->get('subject')->addEventListener(
    FormEvents::POST_SUBMIT,
    function (FormEvent $event) use ($formModifier) {
        // It's important here to fetch $event->getForm()->getData(), as
        // $event->getData() will get you the client data (that is, the ID)
        $subject = $event->getForm()->getData();

        // since we've added the listener to the child, we'll have to pass on
        // the parent to the callback functions!
        $formModifier($event->getForm()->getParent(), $subject);
    }
);
```

> `Assert\Count` me permet d'appliquer une validation et de répondre au besoin de « au moins une case doit être sélectionné »

<div align="justify" class="section">
    Dès lors, dans le résultat de mon appel ajax, j'ajoute ce nouveau bloc à mon formulaire de contact. Je le supprime sinon si le sujet sélectionné est différent que <strong>« Etre rappelé »</strong> :
</div>

```js
// assets/js/contact.js

// ... line 11
$.ajax({
    url : $form.attr('action') + '/' + subject, // will be /contact/{subject}
    type: $form.attr('method'),
    success: function(html) {
        if (subject === 'etre-rappele') {
            // we dynamically add the availabilities block
            var availabilities = $(html).find('#contact_availability').closest('div').parent();
            $('.contact-form-subject-target').after(availabilities);
            
            // we dynamically add "required" attribute for the phone input
            labelForInputPhone.addClass('required');
            inputPhone.attr('required', 'required');         
        } else {    
            // we dynamically remove the availabilities block
            $('#contact_availability').closest('div').parent().remove();

            // we dynamically remove "required" attribute for the phone input
            labelForInputPhone.removeClass('required');
            inputPhone.removeAttr('required');
        }
    }
});
```

<div align="justify" class="section">
    Pour répondre à la problématique <strong>« Le champ téléphone doit être obligatoire dans ce cas précis, il est facultatif sinon. »</strong>, j'ai utilisé <a href="https://symfony.com/doc/current/reference/constraints/Callback.html" title="Callback">l'assertion Callback</a>. Ainsi, dans <a href="https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/src/Entity/Contact.php" title="src/Entity/Contact.php">l'entité Contact</a>, j'ai ajouté une fonction me permettant d'ajouter dynamiquement une contrainte à la propriété <span>phone</span> :
</div>

```php
// App\Entity\Contact.php

// ... line 7
use Symfony\Component\Validator\Context\ExecutionContextInterface;

// ... line 217
/**
 * Dynamically constraints validation
 * 
 * @Assert\Callback
 */
public function validate(ExecutionContextInterface $context, $payload)
{
    if ($this->getSubject() === 'etre-rappele' && empty($this->getPhone())) {
        $context
            ->buildViolation('form.entity.contact.blank.phone')
            ->atPath('phone')
            ->addViolation();
    }
}
```

## 3. Validation

> Si le formulaire n'est pas valide, afficher les messages d'erreur.

Chaque erreur sera retournée par input (en dessous précisemment avec un warning) si une erreur survient. 

> Sinon, il suffira d'afficher un message indiquant que le mail est bien envoyé (ne pas coder l'envoi du mail).

Si tous les champs respectent les règles de validation, alors j'affiche un message flash (success) : 

```php
// App\Controller\ContactController.php

// ... line 53
if ($form->isSubmitted() && $form->isValid()) { // request successfull!
    $this->addFlash('success', 'form.entity.contact.response.success');
    return $this->redirectToRoute('contact_index');
}
```

## 4. Informations complémentaires

> Vous pouvez adapter la mise en forme comme bon vous semble.

<div align="justify" class="section">
    J'ai simplement repris (plus ou moins) l'aspect général du formulaire de contact sur votre site internet.
    Sinon, j'ai utilisé <a href="https://sass-lang.com/" title="sass">sass</a>, entre autre, pour gérer le style.
</div>

> N'hésitez pas à utiliser les bonnes pratiques de codage de Symfony

<div align="justify" class="section">
    A vous de juger... :-)
</div>