<?php

namespace App\Entity;

use App\Validator\Constraints as AppAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Defines the properties of the Contact entity 
 * to represent the application contact requests.
 *
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class Contact
{
    const SUBJECTS = [
        0 => 'Autre',
        1 => 'Contacter le service commercial',
        2 => 'Contacter le service facturation',
        3 => 'Faire une demande sur une association ou un projet solidaire',
        4 => 'Proposer une fonctionnalité',
        5 => 'Je signale un problème technique',
        6 => 'Faire une remarque sur le site',
        7 => 'Être rappelé',
    ];

    const AVAILABILITIES = [
        0 => 'Dès que possible',
        1 => 'Le matin',
        2 => 'Le midi',
        3 => 'L\'après-midi'
    ];

    /**
     * @var string|null
     * 
     * @Assert\NotBlank(message="form.entity.contact.blank.firstname")
     * @Assert\Length(min=2, max=100)
     */
    private $firstname;

    /**
     * @var string|null
     * 
     * @Assert\NotBlank(message="form.entity.contact.blank.lastname")
     * @Assert\Length(min=2, max=100)
     */
    private $lastname;

    /**
     * @var string|null
     * 
     * @AppAssert\Phone()
     */
    private $phone;

    /**
     * @var string|null
     * 
     * @Assert\NotBlank(message="form.entity.contact.blank.email")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * @var array|null
     */
    private $availability;

    /**
     * @var string|null
     * 
     * @Assert\NotBlank(message="form.entity.contact.blank.message")
     * @Assert\Length(min=10, max=500)
     */
    private $message;

    /**
     * @return null|string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param null|string $firstname
     * @return Contact
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param null|string $lastname
     * @return Contact
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param null|string $phone
     * @return Contact
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param null|string $email
     * @return Contact
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param null|string $subject
     * @return Contact
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return null|array
     */
    public function getAvailability(): ?array
    {
        return $this->availability;
    }

    /**
     * @param null|array $availability
     * @return Contact
     */
    public function setAvailability(array $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     * @return Contact
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Dynamically constraints validation
     * 
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->getSubject() === 'etre-rappele' && empty($this->getPhone())) {
            $context
                ->buildViolation('form.entity.contact.blank.phone')
                ->atPath('phone')
                ->addViolation();
        }
    }
}
