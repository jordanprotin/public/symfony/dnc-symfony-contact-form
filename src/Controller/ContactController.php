<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Controller used to manage contact contents.
 *
 * @Route("/contact")
 *
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class ContactController extends AbstractController
{
    /**
     * Print contact form.
     *
     * @Route("/{subject}", methods={"GET", "POST"}, name="contact_index")
     */
    public function index(Request $request, string $subject = 'autre'): Response
    {
        $slugger = new AsciiSlugger();
        $contact = new Contact();

        $subjectChoices = $this->getSubjectChoices();

        // checks if the given subject (through URL parameters)
        // exists within the existing subjects list.
        // default choice = 'autre'
        $isSubjectExists = (string) $slugger
            ->slug(array_search($subject, $subjectChoices))
            ->lower();

        // will go automatically set selected subject in the dropdown list
        // in the form
        $contact->setSubject($isSubjectExists);

        // we give the subjects as options to populate the dropdown list
        // in the form
        $form = $this->createForm(ContactType::class, $contact, [
            'subjectChoices' => $subjectChoices
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { // request successfull!
            $this->addFlash('success', 'form.entity.contact.response.success');
            return $this->redirectToRoute('contact_index');
        }

        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
            'title' => htmlspecialchars('Contactez-nous - Dans Nos Coeurs', ENT_COMPAT | ENT_HTML5),
            'description' => htmlspecialchars('Vous souhaitez faire une remarque, proposer une fonctionnalité, signaler un problème ... ?
            Contactez le service clients dansnoscoeurs via notre formulaire de contact.', ENT_COMPAT | ENT_HTML5)
        ]);
    }

    /**
     * Get the contact subject items 
     * in order to populate the dropdown list
     */
    private function getSubjectChoices()
    {
        $slugger = new AsciiSlugger();
        $choices = Contact::SUBJECTS;

        $output = [];

        foreach($choices as $v) {
            // ex: ['Proposer une fonctionnalité] = 'proposer-une-fonctionnalite'
            $output[$v] = (string) $slugger->slug($v)->lower();
        }

        return $output;
    }
}
