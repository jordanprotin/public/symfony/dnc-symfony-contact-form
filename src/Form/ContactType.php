<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Defines the form used to create and manipulate contact requests.
 * 
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class ContactType extends AbstractType
{
    private $slugger;

    public function __construct()
    {
        $this->slugger = new AsciiSlugger();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'form.entity.contact.label.firstname',
                'attr' => [
                    'autofocus' => true,
                    'minlength' => 2,
                    'maxlength' => 100,
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'form.entity.contact.label.lastname',
                'attr' => [
                    'minlength' => 2,
                    'maxlength' => 100,
                ],
            ])
            ->add('phone', TextType::class, [
                'label' => 'form.entity.contact.label.phone',
                'attr' => [
                    'maxlength' => 14,
                    'placeholder' => '0_.__.__.__.__',
                ],
                'required' => false
            ])
            ->add('email', EmailType::class, [
                'label' => 'form.entity.contact.label.email',
            ])
            ->add('subject', ChoiceType::class, [
                'label' => 'form.entity.contact.label.subject',
                'attr' => [
                    'class' => 'contact-form-subject'
                ],
                'choices' => $options['subjectChoices'],
            ])
            ->add('message', TextareaType::class, [
                'label' => 'form.entity.contact.label.message',
                'attr' => [
                    'rows' => 9,
                    'minlength' => 10,
                    'maxlength' => 500,
                ],
            ])
        ;

        $formModifier = function (FormInterface $form, string $subject = null) {
            $availabilities = null === $subject ? [] : $this->getAvailabilityChoices();

            if ($subject === 'etre-rappele') {
                $form
                    ->add('availability', ChoiceType::class, [
                        'choices' => $availabilities,
                        'multiple' => true,
                        'expanded' => true,
                        'label' => 'form.entity.contact.label.availability',
                        'constraints' => [
                            new Assert\Count([
                                'min' => 1, 
                                'minMessage' => 'form.entity.contact.blank.availability'
                            ])
                        ],
                    ])
                ;
            }
        };
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getSubject());
            }
        );

        $builder->get('subject')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $subject = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $subject);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'subjectChoices' => [],
            'attr' => [
                'novalidate' => 'novalidate' // comment me to reactivate the html5 validation!
            ]
        ]);
    }

    /**
     * Get the contact availability items 
     * in order to populate the dropdown list
     */
    private function getAvailabilityChoices()
    {
        $choices = Contact::AVAILABILITIES;
        $output = [];

        foreach($choices as $v) {
            // ex: ['Dès que possible'] = 'des-que-possible'
            $output[$v] = (string) $this->slugger->slug($v)->lower();
        }

        return $output;
    }
}
