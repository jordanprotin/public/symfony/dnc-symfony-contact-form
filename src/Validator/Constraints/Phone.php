<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * @author Jordan Protin <jordan.protin@yahoo.com>
 */
class Phone extends Constraint
{
    public $message = "Ce numéro de téléphone n'est pas valide.";
}