$(document).ready(function() {
    var $subjectSelect = $('.contact-form-subject');

    // triggered whener a contact subject is selected
    $subjectSelect.on('change', function(e) {
        var $form = $(this).closest('form');
        var subject = $(this).val(); // selected contact subject from the dropdown list
        var inputPhone = $('#contact_phone');
        var labelForInputPhone = inputPhone.closest('div').find('label');

        $.ajax({
            url : $form.attr('action') + '/' + subject, // will be /contact/{subject}
            type: $form.attr('method'),
            success: function(html) {
                if (subject === 'etre-rappele') {
                    // we dynamically add the availabilities block
                    var availabilities = $(html).find('#contact_availability').closest('div').parent();
                    $('.contact-form-subject-target').after(availabilities);
                    
                    // we dynamically add "required" attribute for the phone input
                    labelForInputPhone.addClass('required');
                    inputPhone.attr('required', 'required');         
                } else {    
                    // we dynamically remove the availabilities block
                    $('#contact_availability').closest('div').parent().remove();

                    // we dynamically remove "required" attribute for the phone input
                    labelForInputPhone.removeClass('required');
                    inputPhone.removeAttr('required');
                }
            }
        });
    });
});