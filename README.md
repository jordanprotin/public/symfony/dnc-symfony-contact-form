<div align="center" style="margin-bottom: 30px;">
    <img src="https://www.dansnoscoeurs.fr/images/logo/logo-dnc.png?ea266cace534b37831dca86a8987b5e6cb2ca4e6" alt="logo-dnc">
</div>

> Auteur: Jordan Protin | jordan.protin@yahoo.com

# Dans Nos Coeurs : réponse à un test technique | candidature

<div align="justify">
    Il s'agit d'un petit projet réalisé en [Symfony v5](https://symfony.com/) afin de répondre à un test technique de recrutement concernant la société [dansnoscoeurs.fr](https://www.dansnoscoeurs.fr/). Les consignes sont décrites dans [ce document](https://gitlab.com/jordanprotin/public/dnc-symfony-contact-form/-/blob/master/docs/pdf/exercice_developpeur_symfony.pdf). Le but étant de réaliser un formulaire de contact semblable à [celui-ci](https://www.dansnoscoeurs.fr/contact/contactez-nous) en respectant un certain nombre de règles.
</div>

## Sommaire

  - [Prérequis](#prerequis)
  - [Installation](#installation)
  - [Lancement](#lancement)
  - [Documentation](#documentation)
  
## Prérequis

- [Symfony cli](https://symfony.com/doc/master/cloud/getting-started)
- [Composer](https://getcomposer.org/doc/00-intro.md)
- [Node & Npm](https://www.npmjs.com/get-npm)

## Installation

```bash 
$ git clone git@gitlab.com:jordanprotin/public/dnc-symfony-contact-form.git
$ cd dnc-symfony-contact-form # se déplacer à la racine du projet
$ composer install # installation des dépendances du projet
```

## Lancement

```bash
$ cd dnc-symfony-contact-form # se déplacer à la racine du projet
$ symfony serve # lancement du serveur embarqué de symfony pour plus de facilité
```

> :point_right: Rendez-vous alors sur [http://127.0.0.1:8000/contact](http://127.0.0.1:8000/contact) pour accéder à l'interface développée.

## Documentation

Une documentation a été réalisée afin de décrire les solutions apportées pour répondre aux besoins du projet. Pour lancer cette documentation, veuillez exécuter ces lignes de commandes dans votre terminal à la racine du projet :

```bash
$ cd dnc-symfony-contact-form # se déplacer à la racine du projet
$ npm install # installation des dépendances nécessaires pour pouvoir lancer l'UI de la doc
$ npm run docs 
```

> :+1: votre navigateur web par défaut devrait automatiquement s'ouvrir et vous proposer l'interface de la documentation (réalisée exclusivement avec le format [markdown](https://daringfireball.net/projects/markdown/)).
> Si votre navigateur ne s'ouvre pas automatiquement, rendez-vous directement sur [http://localhost:4040/](http://localhost:4040/).